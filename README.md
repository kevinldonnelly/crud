# CRUD ToDo example in Typescript
## MVC with CRUD controller dependency injection

### Based on
https://github.com/shprink/web-components-todo/tree/master/native-shadow-dom

modified to Typescript, in place editing and CRUD controller added for saving to database

https://en.wikipedia.org/wiki/Create,_read,_update_and_delete

### firebase signin 
##### firebase-signin.ts
```html
<signin-firebase></signin-firebase>
```

### ToDo CRUD Template Data Model and Controller
#### crud.types.ts
``` typescript
interface ICrud<T, K> {
    data: T[];
    Create(value: T, callback: (key: K) => void);
    Read(key: K, callback: (data: T) => void);
    ReadAll(callback: (data: T[]) => void);
    Update(key: K, value: T, callback: () => void);
    Delete(key: K, callback: () => void);
    Keys?(collection: K, callback: (keys: K[]) => void);
}


interface IToDoItem {
    text: string;
    checked: boolean;
    key?: string;
    order?: number;
}
```
#### React todo example that uses see similair pattern at
https://github.com/microsoft/frontend-bootcamp/blob/master/step2-06/exercise/src/actions/index.ts


### Firebase implementation 
#### crud.firebase.ts
``` javascript
element = document.querySelector('signin-firebase');
element.config = firebaseConfig;

export namespace Firebase {

    export class TodoCrud implements ICrud<IToDoItem, string> {

```
### crud dependency injection 
#### main.ts
``` javascript

todo = document.querySelector('my-todo');

let crud = new Firebase.TodoCrud('0', () => {
      todo.crud = crud; // todo list rendered from list array
   }); 

```

#### my-todo.ts
``` typescript

 _crud: ICrud<IToDoItem, string>;

 set crud(value: ICrud<IToDoItem, number>) {

        this._crud = value;
        this._render();
    }
```


