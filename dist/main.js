/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _my_todo__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./my-todo */ \"./src/my-todo.js\");\n/* harmony import */ var _todo_input__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./todo-input */ \"./src/todo-input.js\");\n/* harmony import */ var _todo_item__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./todo-item */ \"./src/todo-item.js\");\n\n\n\n\nwindow.customElements.define('my-todo', _my_todo__WEBPACK_IMPORTED_MODULE_0__[\"default\"]);\nwindow.customElements.define('todo-input', _todo_input__WEBPACK_IMPORTED_MODULE_1__[\"default\"]);\nwindow.customElements.define('todo-item', _todo_item__WEBPACK_IMPORTED_MODULE_2__[\"default\"]);\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ }),

/***/ "./src/my-todo.js":
/*!************************!*\
  !*** ./src/my-todo.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return MyTodo; });\nconst templateTodo = document.createElement('template');\ntemplateTodo.innerHTML = `\n    <style>\n        h1 {\n            font-size: 100px;\n            font-weight: 100;\n            text-align: center;\n            color: rgba(175, 47, 47, 0.15);\n        }\n\n        section {\n            background: #fff;\n            margin: 30px 0 40px 0;\n            position: relative;\n            box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2), 0 25px 50px 0 rgba(0, 0, 0, 0.1);\n        }\n\n        #list-container {\n            margin: 0;\n            padding: 0;\n            list-style: none;\n            border-top: 1px solid #e6e6e6;\n        }\n    </style>\n    <h1>Todos WC</h1>\n    <section>\n        <todo-input></todo-input>\n        <ul id=\"list-container\"></ul>\n    </section>\n`;\n\nclass MyTodo extends HTMLElement {\n    constructor() {\n        super();\n        this._root = this.attachShadow({ 'mode': 'open' });\n        // initial state\n        this._list = [{ text: 'my initial todo', checked: false }, { text: 'Learn about Web Components', checked: true }];\n    }\n\n    connectedCallback() {\n        this._root.appendChild(templateTodo.content.cloneNode(true));\n        this.$input = this._root.querySelector('todo-input');\n        this.$listContainer = this._root.querySelector('#list-container');\n        this.$input.addEventListener('onSubmit', this.addItem.bind(this));\n        this._render();\n    }\n\n    addItem(e) {\n        this._list.push({ text: e.detail, checked: false });\n        this._render();\n    }\n\n    removeItem(e) {\n        this._list.splice(e.detail, 1);\n        this._render();\n    }\n\n    toggleItem(e) {\n        const item = this._list[e.detail];\n        this._list[e.detail] = Object.assign({}, item, {\n            checked: !item.checked\n        });\n        this._render();\n    }\n\n    disconnectedCallback() {}\n\n    _render() {\n        if (!this.$listContainer) return;\n        // empty the list\n        this.$listContainer.innerHTML = '';\n        this._list.forEach((item, index) => {\n            let $item = document.createElement('todo-item');\n            $item.setAttribute('text', item.text);\n            $item.checked = item.checked;\n            $item.index = index;\n            $item.addEventListener('onRemove', this.removeItem.bind(this));\n            $item.addEventListener('onToggle', this.toggleItem.bind(this));\n            this.$listContainer.appendChild($item);\n        });\n    }\n}\n\n//# sourceURL=webpack:///./src/my-todo.js?");

/***/ }),

/***/ "./src/todo-input.js":
/*!***************************!*\
  !*** ./src/todo-input.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return TodoInput; });\nconst templateTodoInput = document.createElement('template');\ntemplateTodoInput.innerHTML = `\n    <style>\n        #new-todo-form {\n            position: relative;\n            font-size: 24px;\n            border-bottom: 1px solid #ededed;\n        }\n\n        #new-todo {\n            padding: 16px 16px 16px 60px;\n            border: none;\n            background: rgba(0, 0, 0, 0.003);\n            position: relative;\n            margin: 0;\n            width: 100%;\n            font-size: 24px;\n            font-family: inherit;\n            font-weight: inherit;\n            line-height: 1.4em;\n            border: 0;\n            outline: none;\n            color: inherit;\n            padding: 6px;\n            border: 1px solid #CCC;\n            box-shadow: inset 0 -1px 5px 0 rgba(0, 0, 0, 0.2);\n            box-sizing: border-box;\n        }\n    </style>\n    <form id=\"new-todo-form\">\n        <input id=\"new-todo\" type=\"text\" placeholder=\"What needs to be done?\" />\n    </form>\n`;\n\nclass TodoInput extends HTMLElement {\n    constructor() {\n        super();\n        this._root = this.attachShadow({ 'mode': 'open' });\n    }\n\n    connectedCallback() {\n        console.log('TodoInput ADDED TO THE DOM');\n        this._root.appendChild(templateTodoInput.content.cloneNode(true));\n        this.$form = this._root.querySelector('form');\n        this.$input = this._root.querySelector('input');\n        this.$form.addEventListener(\"submit\", e => {\n            e.preventDefault();\n            if (!this.$input.value) return;\n            this.dispatchEvent(new CustomEvent('onSubmit', { detail: this.$input.value }));\n            this.$input.value = '';\n        });\n    }\n\n    disconnectedCallback() {\n        console.log('TodoInput REMOVED TO THE DOM');\n    }\n}\n\n//# sourceURL=webpack:///./src/todo-input.js?");

/***/ }),

/***/ "./src/todo-item.js":
/*!**************************!*\
  !*** ./src/todo-item.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return TodoItem; });\nconst templateTodoItem = document.createElement('template');\ntemplateTodoItem.innerHTML = `\n    <style>\n      :host {\n        display: block;\n      }\n\n      li.item {\n        font-size: 24px;\n        display: block;\n        position: relative;\n        border-bottom: 1px solid #ededed;\n      }\n\n      li.item input {\n        text-align: center;\n        width: 40px;\n        /* auto, since non-WebKit browsers doesn't support input styling */\n        height: auto;\n        position: absolute;\n        top: 9px;\n        bottom: 0;\n        margin: auto 0;\n        border: none;\n        /* Mobile Safari */\n        -webkit-appearance: none;\n        appearance: none;\n      }\n\n      li.item input:after {\n        content: url('data:image/svg+xml;utf8,<svg%20xmlns%3D\"http%3A//www.w3.org/2000/svg\"%20width%3D\"40\"%20height%3D\"40\"%20viewBox%3D\"-10%20-18%20100%20135\"><circle%20cx%3D\"50\"%20cy%3D\"50\"%20r%3D\"50\"%20fill%3D\"none\"%20stroke%3D\"%23ededed\"%20stroke-width%3D\"3\"/></svg>');\n      }\n\n      li.item input:checked:after {\n        content: url('data:image/svg+xml;utf8,<svg%20xmlns%3D\"http%3A//www.w3.org/2000/svg\"%20width%3D\"40\"%20height%3D\"40\"%20viewBox%3D\"-10%20-18%20100%20135\"><circle%20cx%3D\"50\"%20cy%3D\"50\"%20r%3D\"50\"%20fill%3D\"none\"%20stroke%3D\"%23bddad5\"%20stroke-width%3D\"3\"/><path%20fill%3D\"%235dc2af\"%20d%3D\"M72%2025L42%2071%2027%2056l-4%204%2020%2020%2034-52z\"/></svg>');\n      }\n\n      li.item label {\n        white-space: pre;\n        word-break: break-word;\n        padding: 15px 60px 15px 15px;\n        margin-left: 45px;\n        display: block;\n        line-height: 1.2;\n        transition: color 0.4s;\n      }\n\n      li.item.completed label {\n        color: #d9d9d9;\n        text-decoration: line-through;\n      }\n\n      li.item button,\n      li.item input[type=\"checkbox\"] {\n        outline: none;\n      }\n\n      li.item button {\n        margin: 0;\n        padding: 0;\n        border: 0;\n        background: none;\n        font-size: 100%;\n        vertical-align: baseline;\n        font-family: inherit;\n        font-weight: inherit;\n        color: inherit;\n        -webkit-appearance: none;\n        appearance: none;\n        -webkit-font-smoothing: antialiased;\n        -moz-font-smoothing: antialiased;\n        font-smoothing: antialiased;\n      }\n\n      li.item .destroy {\n        position: absolute;\n        top: 0;\n        right: 10px;\n        bottom: 0;\n        width: 40px;\n        height: 40px;\n        margin: auto 0;\n        font-size: 30px;\n        color: #cc9a9a;\n        margin-bottom: 11px;\n        transition: color 0.2s ease-out;\n      }\n\n      li.item .destroy:hover {\n        color: #af5b5e;\n      }\n    </style>\n    <li class=\"item\">\n        <input type=\"checkbox\">\n        <label></label>\n        <button class=\"destroy\">x</button>\n    </li>\n`;\n\nclass TodoItem extends HTMLElement {\n    constructor() {\n        super();\n        this._root = this.attachShadow({ 'mode': 'open' });\n        this._checked = this.checked;\n        this._index = this.index;\n        this._text = '';\n    }\n    connectedCallback() {\n        this._root.appendChild(templateTodoItem.content.cloneNode(true));\n        this.$item = this._root.querySelector('.item');\n        this.$removeButton = this._root.querySelector('.destroy');\n        this.$text = this._root.querySelector('label');\n        this.$checkbox = this._root.querySelector('input');\n        this.$removeButton.addEventListener('click', e => {\n            e.preventDefault();\n            this.dispatchEvent(new CustomEvent('onRemove', { detail: this.index }));\n        });\n        this.$checkbox.addEventListener('click', e => {\n            e.preventDefault();\n            this.dispatchEvent(new CustomEvent('onToggle', { detail: this.index }));\n        });\n        this._render();\n    }\n    disconnectedCallback() {}\n    static get observedAttributes() {\n        return ['text'];\n    }\n    attributeChangedCallback(name, oldValue, newValue) {\n        this._text = newValue;\n    }\n    set index(value) {\n        this._index = value;\n    }\n    get index() {\n        return this._index;\n    }\n    set checked(value) {\n        this._checked = Boolean(value);\n    }\n    get checked() {\n        return this.hasAttribute('checked');\n    }\n    _render() {\n        if (!this.$item) return;\n        this.$text.textContent = this._text;\n        if (this._checked) {\n            this.$item.classList.add('completed');\n            this.$checkbox.setAttribute('checked', '');\n        } else {\n            this.$item.classList.remove('completed');\n            this.$checkbox.removeAttribute('checked');\n        }\n    }\n}\n\n//# sourceURL=webpack:///./src/todo-item.js?");

/***/ })

/******/ });