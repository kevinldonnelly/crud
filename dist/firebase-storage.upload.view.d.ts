import 'firebase/storage';
export default class FirebaseStorage extends HTMLElement {
    prov: Element;
    _folder: string;
    accept: string;
    selector: string;
    _controller: IStorageController;
    constructor();
    folder: string;
    controller: IStorageController;
    updateItems(): void;
    deleteChild(parent: HTMLElement): void;
    connectedCallback(): void;
    static readonly observedAttributes: string[];
    attributeChangedCallback(name: string, oldValue: string, newValue: string): void;
}
