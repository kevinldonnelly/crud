export default class TodoItem extends HTMLElement {
    _root: any;
    _checked: boolean;
    _index: number;
    _text: string;
    $item: Element;
    $removeButton: HTMLButtonElement;
    $text: HTMLLabelElement;
    $checkbox: HTMLInputElement;
    constructor();
    connectedCallback(): void;
    disconnectedCallback(): void;
    static readonly observedAttributes: string[];
    attributeChangedCallback(name: string, oldValue: string, newValue: string): void;
    index: any;
    checked: any;
    _render(): void;
}
