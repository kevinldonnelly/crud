import { Firebase } from '../crud.firebase';
import TodoInput from './todo-input';
import 'firebase/auth';
export default class MyTodo extends HTMLElement {
    _root: any;
    _list: Firebase.TodoCrud;
    $input: TodoInput;
    $listContainer: Element;
    constructor();
    connectedCallback(): void;
    addItem(e: any): void;
    removeItem(e: any): void;
    toggleItem(e: any): void;
    updateItem(e: any): void;
    disconnectedCallback(): void;
    _render(): void;
}
