import 'firebase/auth';
export default class FirebaseSignIn extends HTMLElement implements IUser {
    firebaseConfig: IfirebaseConfig;
    idToken: string;
    _token: string;
    readonly userid: string;
    readonly token: string;
    readonly token_id: string;
    readonly displayname: string;
    config: IfirebaseConfig;
    constructor();
    connectedCallback(): void;
    static readonly observedAttributes: string[];
    attributeChangedCallback(name: string, oldValue: string, newValue: string): void;
    signInWithGoogle(): void;
    signInWithFaceBook(): void;
    signOut(): void;
    StartListen(): void;
}
