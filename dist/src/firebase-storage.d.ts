import 'firebase/storage';
export default class FirebaseStorage extends HTMLElement {
    prov: Element;
    folder: string;
    accept: string;
    selector: string;
    constructor();
    updateItems(): void;
    deleteChild(parent: HTMLElement): void;
    connectedCallback(): void;
    static readonly observedAttributes: string[];
    attributeChangedCallback(name: string, oldValue: string, newValue: string): void;
    ListFiles(folder: string): IStorage;
}
