import 'firebase/auth';
export default class FirebaseSignIn extends HTMLElement {
    firebaseConfig: IfirebaseConfig;
    config: IfirebaseConfig;
    constructor();
    connectedCallback(): void;
    static readonly observedAttributes: string[];
    attributeChangedCallback(name: string, oldValue: string, newValue: string): void;
    signInWithCredential(token: string, callback: (msg: string) => void): void;
    signInWithGoogle(): void;
    signInWithFaceBook(): void;
    signOut(): void;
    StartListen(): void;
}
