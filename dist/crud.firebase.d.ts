import TodoInput from './todo-input';
import 'firebase/database';
import 'firebase/storage';
import './crud.types';
interface IPath {
    name: string;
    url: string;
}
export declare namespace Firebase {
    class TodoCrud extends HTMLElement implements ICrud<IToDoItem, string> {
        data: Array<IToDoItem>;
        _root: any;
        _name: string;
        $input: TodoInput;
        $listContainer: Element;
        collection: string;
        path: string;
        user_paths: Array<IPath>;
        public_paths: Array<IPath>;
        constructor();
        connectedCallback(): void;
        addItem(e: CustomEvent): void;
        _render(): void;
        Initialize(): void;
        name(name: string, callback: () => void): void;
        ReadAll(callback: (data: IToDoItem[]) => void): void;
        Create(value: IToDoItem, callback: (key: string) => void): void;
        Read(key: string, callback: (data: IToDoItem) => void): void;
        Update(key: string, value: IToDoItem, callback: () => void): void;
        Delete(key: string, callback: () => void): void;
        Keys(collection: string, callback: (keys: any) => void): void;
    }
}
export {};
