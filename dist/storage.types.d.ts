interface IFile {
    Name: string;
    id?: string;
    Extension?: string;
    FileSizeBytes?: Number;
    IsTextFile?: boolean;
    fullPath?: string;
    ModifiedDate?: Date;
    Contents?: string;
    metadata?: Object;
}
interface IFolder {
    Name: string;
    id?: string;
    FileCount?: Number;
    ModifiedDate?: Date;
    fullPath?: string;
}
interface IStorage {
    Parent?: IFolder | string;
    Home?: IFolder;
    File?: IFile;
    Files: IFile[];
    Folders: IFolder[];
}
interface ISnapshot {
    bytesTransferred: number;
    totalBytes: number;
}
interface IStorageController {
    folder: IFolder;
    Delete(file: IFile, callback: (msg: string) => void): any;
    ListAll(folder: IFolder, callback: (data: IStorage) => void): any;
    MkDir(file: IFile, callback: (msg: string) => void): any;
    Upload(file: IFile, data: File, callback: (snapshot: ISnapshot) => void): any;
}
