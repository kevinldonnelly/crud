export default class StorageUploadView extends HTMLElement {
    prov: Element;
    accept: string;
    selector: string;
    _controller: IStorageController;
    constructor();
    controller: IStorageController;
    updateItems(): void;
    deleteChild(parent: HTMLElement): void;
    connectedCallback(): void;
    static readonly observedAttributes: string[];
    attributeChangedCallback(name: string, oldValue: string, newValue: string): void;
}
