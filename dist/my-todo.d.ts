import TodoInput from './todo-input';
export default class MyTodo extends HTMLElement {
    _root: any;
    _crud: ICrud<IToDoItem, string>;
    $input: TodoInput;
    $listContainer: Element;
    constructor();
    connectedCallback(): void;
    crud: ICrud<IToDoItem, string>;
    addItem(e: CustomEvent): void;
    removeItem(e: CustomEvent): void;
    toggleItem(e: CustomEvent): void;
    updateItem(e: CustomEvent): void;
    disconnectedCallback(): void;
    _render(): void;
}
