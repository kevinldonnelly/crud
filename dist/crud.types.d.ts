interface ICrud<T, K> {
    data: T[];
    Create(value: T, callback: (key: K) => void): any;
    Read(key: K, callback: (data: T) => void): any;
    ReadAll(callback: (data: T[]) => void): any;
    Update(key: K, value: T, callback: () => void): any;
    Delete(key: K, callback: () => void): any;
    Keys?(collection: K, callback: (keys: K[]) => void): any;
}
interface IToDoItem {
    text: string;
    checked: boolean;
    key?: string;
    order?: number;
}
interface IfirebaseConfig {
    apiKey: string;
    authDomain: string;
    databaseURL: string;
    appId: string;
    measurementId: string;
    storageBucket?: string;
}
