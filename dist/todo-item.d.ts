export default class TodoItem extends HTMLElement {
    _root: any;
    _index: number;
    _todo_item: IToDoItem;
    dragItem: any[];
    dropItem: any[];
    $item: Element;
    $removeButton: HTMLButtonElement;
    $text: HTMLLabelElement;
    $checkbox: HTMLInputElement;
    constructor();
    connectedCallback(): void;
    disconnectedCallback(): void;
    static readonly observedAttributes: string[];
    attributeChangedCallback(name: string, oldValue: string, newValue: string): void;
    index: any;
    checked: any;
    key: string;
    _render(): void;
}
