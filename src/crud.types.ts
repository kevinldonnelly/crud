
interface ICrud<T, K> {
    data: T[];
    Create(value: T, callback: (key: K) => void);
    Read(key: K, callback: (data: T) => void);
    ReadAll(callback: (data: T[]) => void);
    Update(key: K, value: T, callback: () => void);
    Delete(key: K, callback: () => void);
    Keys?(collection: K, callback: (keys: K[]) => void);
}

interface IToDoItem {
    text: string;
    checked: boolean;
    key?: string;
    order?: number;
}



interface IfirebaseConfig {
    apiKey: string;
    authDomain: string;
    databaseURL: string;
    appId: string;
    measurementId: string;
    storageBucket?: string;
}


