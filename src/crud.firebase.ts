
import firebase, { database } from 'firebase/app';
import TodoInput from './todo-input';

import 'firebase/database';

import 'firebase/storage';

import './crud.types';

interface IPath {
    name: string;
    url: string;
}

const templateCrud = document.createElement('template');
templateCrud.innerHTML = `
    <style>
        h1 {
            font-size: 100px;
            font-weight: 100;
            text-align: center;
            color: rgba(175, 47, 47, 0.15);
        }

        section {
            background: #fff;
            margin: 30px 0 40px 0;
            position: relative;
            box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2), 0 25px 50px 0 rgba(0, 0, 0, 0.1);
        }

        #list-container {
            margin: 0;
            padding: 0;
            list-style: none;
            border-top: 1px solid #e6e6e6;
        }
    </style>
    <h1>CRUD WC</h1>
    <section>
        <todo-input></todo-input>
        <ul id="list-container"></ul>
    </section>
`;


export namespace Firebase {

    export class TodoCrud extends HTMLElement implements ICrud<IToDoItem, string> {

        data: Array<IToDoItem> = new Array<IToDoItem>();
        _root: any;
        _name: string;
        $input: TodoInput;
        $listContainer: Element;
        collection: string;
        path: string;
        user_paths: Array<IPath> = new Array<IPath>();
        public_paths: Array<IPath> = new Array<IPath>();

        constructor() {
            super();
            this._root = this.attachShadow({ 'mode': 'open' });
            this.collection = 'Todos';

        }

        connectedCallback() {
            this._root.appendChild(templateCrud.content.cloneNode(true));
            this.$input = this._root.querySelector('todo-input');
            this.$listContainer = this._root.querySelector('#list-container');
            this.$input.addEventListener('onSubmit', this.addItem.bind(this));
            this.addEventListener('onUpdate', this._render.bind(this));
            //   this._render();
        }

        addItem(e: CustomEvent) {
            var $item = document.createElement('li');
            $item.innerText = e.detail;
            this.$listContainer.appendChild($item);
            $item.addEventListener('click', (e: MouseEvent) => {
                const items = Array.from(this._root.querySelectorAll('li')) as HTMLElement[];
                items.forEach(item => {
                    item.style.backgroundColor = 'white';
                });
                let ele: HTMLElement = e.target as HTMLElement;
                ele.style.backgroundColor = '#bad0e4';
                this.dispatchEvent(new CustomEvent('onSelectionChange', { detail: ele.innerText }));
            });
        }

        _render() {
            if (!this.$listContainer) return;
            // empty the list
            if (this.$input.$input != null) {
                this.$input.$input.placeholder = 'new todo list';
            }
            this.$listContainer.innerHTML = '';


            this.Keys('Todos', (keys) => {
                console.log(keys);
                for (var key in keys) {
                    var $item = document.createElement('li');
                    $item.innerText = key;
                    this.$listContainer.appendChild($item);


                    $item.addEventListener('click', (e: MouseEvent) => {
                        const items = Array.from(this._root.querySelectorAll('li')) as HTMLElement[];
                        items.forEach(item => {
                            item.style.backgroundColor = 'white';
                        });
                        let ele: HTMLElement = e.target as HTMLElement;
                        ele.style.backgroundColor = '#bad0e4';
                        this.dispatchEvent(new CustomEvent('onSelectionChange', { detail: ele.innerText }));
                    });
                }


            });
        }

        Initialize() {
            let path: IPath = { name: 'UID', url: `/users/${firebase.auth().currentUser.uid}` };
            this.path = path.url;
            this.user_paths.push(path);
            path = { name: 'email', url: `/users/${firebase.auth().currentUser.email}` };
            this.user_paths.push(path);
            path = { name: 'public', url: `/public` };
            this.public_paths.push(path);
            this._render();
        }

        name(name: string, callback: () => void) {
            this._name = name;

            this.ReadAll(callback);
         //   this._render();
        }

        ReadAll(callback: (data: IToDoItem[]) => void) {

            if (!firebase.auth().currentUser) {

                console.log('no current user');
                return;
            }

            async function get(name: string, collection: string, path: string) {

                let data: Array<IToDoItem> = new Array<IToDoItem>();
                let promise = await firebase.database().ref(`${path}/${collection}/${name}`).once('value');

                console.log(`${path}/${collection}/${name}`);

                promise.forEach(element => {
                    let item: IToDoItem = { text: '', checked: false };
                    var value = element.val();
                    item.text = value.ToDo;
                    item.checked = value.checked;
                    item.key = element.key;
                    data.push(item);
                });

                return data;

            }

            get(this._name, this.collection, this.path).then(data => {

                this.data = data;
                callback(data);

            });
        }


        Create(value: IToDoItem, callback: (key: string) => void) {

            if (!firebase.auth().currentUser) return;
            var newToDoKey = firebase.database().ref().child(`${this.path}/${this.collection}/${this._name}`).push().key;
            value.key = newToDoKey;

            firebase.database().ref(`${this.path}/${this.collection}/${this._name}/${newToDoKey}`).update({
                'ToDo': value.text
            }).then(() => {
                this.data.push(value);
                callback(newToDoKey);
            });

        }

        Read(key: string, callback: (data: IToDoItem) => void) {
            if (!firebase.auth().currentUser) return;

            firebase.database().ref(`${this.path}/${this.collection}/${this._name}/${key}`).once('value').then((snapshot) => {
                var value = snapshot.val();
                callback(value);
            });

        }

        Update(key: string, value: IToDoItem, callback: () => void) {
            if (!firebase.auth().currentUser) return;
            //  console.log(value);
            //  return;
            firebase.database().ref(`${this.path}/${this.collection}/${this._name}/${key}`).update({
                'ToDo': value.text,
                'checked': value.checked
            }, (error) => {
                if (error) {
                    // The write failed...
                } else {
                    console.log(' Data saved successfully!');
                    //  this.data[index].text = value.text;
                    this.data.forEach((item, index) => {
                        if (item.key === key) {
                            this.data[index].text = value.text;
                            this.data[index].checked = value.checked;
                        }
                    });
                    callback();
                    //  this.element.dispatchEvent(new Event('onUpdate'));
                }
            }

            );

        }

        Delete(key: string, callback: () => void) {

            firebase.database().ref(`${this.path}/${this.collection}/${this._name}/${key}`).remove().then(() => {
                //  this.data.splice(index, 1);
                for (var i = this.data.length - 1; i >= 0; i--) {
                    if (this.data[i].key === key) {
                        this.data.splice(i, 1);
                    }
                }

                callback();
            });

        }

        Keys(collection: string, callback: (keys: any) => void) {

            if (!firebase.auth().currentUser) return;

            // https://firebase.google.com/docs/database/rest/auth
            // https://firebase.google.com/docs/database/rest/retrieve-data#shallow
            firebase.auth().currentUser.getIdToken(true).then(idtoken => {
                fetch(firebase.apps[0].options['databaseURL'] + `${this.path}/${collection}.json?shallow=true&auth=${idtoken}`)
                    .then((response) => {
                        return response.json();
                    })
                    .then((keys) => {
                        callback(keys);
                    });
            });

        }

    }
}