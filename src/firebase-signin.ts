import firebase from 'firebase/app';
import 'firebase/auth';


export default class FirebaseSignIn extends HTMLElement {


    firebaseConfig: IfirebaseConfig;

    set config(value: IfirebaseConfig) {

        this.firebaseConfig = value;
        this.StartListen();
    }

    get config(): IfirebaseConfig {
        return this.firebaseConfig;
    }

    constructor() {
        super();

        //  this.attachShadow({ mode: 'open' });
        //  this.shadowRoot.appendChild(template.content.cloneNode(true));
    }

    connectedCallback() {

    }

    static get observedAttributes() {
        return ['config'];
    }

    attributeChangedCallback(name: string, oldValue: string, newValue: string) {

    }

    // https://stackoverflow.com/questions/45001120/how-to-get-providers-access-tokens-from-firebase-authenticated-user/45027069#45027069

    signInWithCredential(token: string, callback: (msg: string) => void) {


        if (typeof token === 'undefined') return;

        console.log(token);

        var credential = firebase.auth.GoogleAuthProvider.credential(token);

        firebase.auth().signInWithCredential(credential).catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            if (errorCode === 'auth/account-exists-with-different-credential') {
                alert('Email already associated with another account.');
                // Handle account linking here, if using.
            } else {
                console.error(error);
            }
        }).then(() => {

            callback('ok');

        });

    }
    signInWithGoogle() {
        var provider = new firebase.auth.GoogleAuthProvider();


        firebase.auth().signInWithPopup(provider).then((result: any) => {

        });

    }
    signInWithFaceBook() {
        var provider = new firebase.auth.FacebookAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function (result: any) {
        });

    }


    signOut() {
        firebase.auth().signOut();
    }

    StartListen() {

        try {
            firebase.initializeApp(this.firebaseConfig);

            firebase.auth().onAuthStateChanged((user) => {

                this.dispatchEvent(new Event('onchange'));
            });
        }
        catch (errorCode) {
            console.log(errorCode);
        }

    }
}