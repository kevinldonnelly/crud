import MyTodo from './my-todo';
import TodoInput from './todo-input';
import TodoItem from './todo-item';


import '@kevinldonnelly/login-google';

import FirebaseSignIn from './firebase-signin';

import { Firebase } from './crud.firebase';

import firebase from 'firebase/app';

import '@vaadin/vaadin-split-layout'

customElements.define('my-todo', MyTodo);
customElements.define('todo-input', TodoInput);
customElements.define('todo-item', TodoItem);

customElements.define('signin-firebase', FirebaseSignIn);

customElements.define('crud-firebase', Firebase.TodoCrud);


var firebaseConfig: IfirebaseConfig = {
    apiKey: 'AIzaSyBbJ3TUQsjxu0dMcGd4A3AN_Hj3KbGBvPs',
    authDomain: 'soultech-signage.firebaseapp.com',
    databaseURL: 'https://soultech-signage.firebaseio.com',
    appId: '1:629054288068:web:bdcf4da2b742a403efd991',
    measurementId: 'G-RQNNGD71XQ',
    storageBucket: 'soultech-signage.appspot.com'
};

function setloginbtn(signedin: boolean) {
    var but: HTMLElement = document.querySelector('#login');
    if (signedin) but.innerHTML = 'Sign Out';
    else but.innerHTML = 'Sign In';
}

window.addEventListener('load', (event) => {
    var gelement: any;
    var felement: any;
    var todo: MyTodo;
    var crud: Firebase.TodoCrud;

    console.log('window loaded');

    gelement = document.querySelector('login-google');

    felement = document.querySelector('signin-firebase');



    gelement.addEventListener('onchange', () => {

        felement.config = firebaseConfig;
        setloginbtn(gapi.auth2.GoogleAuth['isSignedIn'].get());

        console.log(firebase.apps[0].options['databaseURL']);

        felement.addEventListener('onchange', () => {

            todo = document.querySelector('my-todo');

            crud = document.querySelector('crud-firebase');

            crud.Initialize();
            todo.crud = crud;
            
            crud.addEventListener('onSelectionChange', (e: CustomEvent) => {
            
                crud.name(e.detail, () => {
                    todo._render();
                });

            });


            console.log(`firebase signin change ${firebase.auth().currentUser.displayName}`);
        });

        // Use gapi token to sign into firebase
        felement.signInWithCredential(gapi.auth2.GoogleAuth['currentUser'].get().getAuthResponse().id_token, (msg) => {

            console.log('signInWithCredential=' + msg);
        });

    });

    var but = document.querySelector('#login');

    but.addEventListener('click', (e) => {
        console.log(gapi.auth2.GoogleAuth);
        if (but.innerHTML === 'Sign In') gapi.auth2.GoogleAuth['signIn']();
        else gapi.auth2.GoogleAuth['signOut']();
        setloginbtn(gapi.auth2.GoogleAuth['isSignedIn'].get());
    });





});