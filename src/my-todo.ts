import TodoInput from './todo-input';
import TodoItem from './todo-item';



const templateTodo = document.createElement('template');
templateTodo.innerHTML = `
    <style>
        h1 {
            font-size: 100px;
            font-weight: 100;
            text-align: center;
            color: rgba(175, 47, 47, 0.15);
        }

        section {
            background: #fff;
            margin: 30px 0 40px 0;
            position: relative;
            box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2), 0 25px 50px 0 rgba(0, 0, 0, 0.1);
        }

        #list-container {
            margin: 0;
            padding: 0;
            list-style: none;
            border-top: 1px solid #e6e6e6;
        }
    </style>
    <h1>Todos WC</h1>
    <section>
        <todo-input></todo-input>
        <ul id="list-container"></ul>
    </section>
`;
export default class MyTodo extends HTMLElement {
    _root: any;
    _crud: ICrud<IToDoItem, string>;
    $input: TodoInput;
    $listContainer: Element;
    constructor() {
        super();
        this._root = this.attachShadow({ 'mode': 'open' });
    }

    connectedCallback() {
        this._root.appendChild(templateTodo.content.cloneNode(true));
        this.$input = this._root.querySelector('todo-input');
        this.$listContainer = this._root.querySelector('#list-container');
        this.$input.addEventListener('onSubmit', this.addItem.bind(this));
        this.addEventListener('onUpdate', this._render.bind(this));
    }

    set crud(value: ICrud<IToDoItem, string>) {

        this._crud = value;
      //  this._render();
    }

    addItem(e: CustomEvent) {
        this._crud.Create({ text: e.detail, checked: false, }, () => {
            this._render();
        });

    }

    removeItem(e: CustomEvent) {
        const item: IToDoItem = e.detail;
        this._crud.Delete(item.key, () => {
            this._render();
        });

    }

    toggleItem(e: CustomEvent) {
        const item: IToDoItem = e.detail;
        this._crud.Update(item.key, item, () => {
            this._render();
        });

    }

    updateItem(e: CustomEvent) {
        const item: IToDoItem = e.detail;
        this._crud.Update(item.key, item, () => {
            this._render();
        });

    }

    disconnectedCallback() { }

    _render() {
        if (!this.$listContainer) return;
        // empty the list
        if (this.$input.$input != null) {
            this.$input.$input.placeholder = 'What needs to be done,?';
        }
        this.$listContainer.innerHTML = '';

        this._crud.data.forEach((item, index) => {

            var $item = document.createElement('todo-item') as TodoItem;
            $item.setAttribute('text', item.text);
            $item.checked = item.checked;
            $item.key = item.key;
            $item.index = index;
            $item.addEventListener('onRemove', this.removeItem.bind(this));
            $item.addEventListener('onToggle', this.toggleItem.bind(this));
            this.$listContainer.appendChild($item);
            $item.addEventListener('onUpdate', this.updateItem.bind(this));
        });
    }
}


