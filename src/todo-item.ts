const templateTodoItem = document.createElement('template');
templateTodoItem.innerHTML = `
    <style>
      :host {
        display: block;
      }

      li.item {
        font-size: 24px;
        display: block;
        position: relative;
        border-bottom: 1px solid #ededed;
      }

      li.item input {
        text-align: center;
        width: 40px;
        /* auto, since non-WebKit browsers doesn't support input styling */
        height: auto;
        position: absolute;
        top: 9px;
        bottom: 0;
        margin: auto 0;
        border: none;
        /* Mobile Safari */
        -webkit-appearance: none;
        appearance: none;
      }

      li.item input:after {
        content: url('data:image/svg+xml;utf8,<svg%20xmlns%3D"http%3A//www.w3.org/2000/svg"%20width%3D"40"%20height%3D"40"%20viewBox%3D"-10%20-18%20100%20135"><circle%20cx%3D"50"%20cy%3D"50"%20r%3D"50"%20fill%3D"none"%20stroke%3D"%23ededed"%20stroke-width%3D"3"/></svg>');
      }

      li.item input:checked:after {
        content: url('data:image/svg+xml;utf8,<svg%20xmlns%3D"http%3A//www.w3.org/2000/svg"%20width%3D"40"%20height%3D"40"%20viewBox%3D"-10%20-18%20100%20135"><circle%20cx%3D"50"%20cy%3D"50"%20r%3D"50"%20fill%3D"none"%20stroke%3D"%23bddad5"%20stroke-width%3D"3"/><path%20fill%3D"%235dc2af"%20d%3D"M72%2025L42%2071%2027%2056l-4%204%2020%2020%2034-52z"/></svg>');
      }

      li.item label {
        white-space: pre;
        word-break: break-word;
        padding: 15px 60px 15px 15px;
        margin-left: 45px;
        display: block;
        line-height: 1.2;
        transition: color 0.4s;
      }

      li.item.completed label {
        color: #d9d9d9;
        text-decoration: line-through;
      }

      li.item button,
      li.item input[type="checkbox"] {
        outline: none;
      }

      li.item button {
        margin: 0;
        padding: 0;
        border: 0;
        background: none;
        font-size: 100%;
        vertical-align: baseline;
        font-family: inherit;
        font-weight: inherit;
        color: inherit;
        -webkit-appearance: none;
        appearance: none;
        -webkit-font-smoothing: antialiased;
        -moz-font-smoothing: antialiased;
        font-smoothing: antialiased;
      }

      li.item .destroy {
        position: absolute;
        top: 0;
        right: 10px;
        bottom: 0;
        width: 40px;
        height: 40px;
        margin: auto 0;
        font-size: 30px;
        color: #cc9a9a;
        margin-bottom: 11px;
        transition: color 0.2s ease-out;
      }

      li.item .destroy:hover {
        color: #af5b5e;
      }
    </style>
    <li class="item" draggable="true" id="dragtarget">
        <input type="checkbox">
        <label contenteditable="true" draggable="true" id="dragtarget" class="droptarget"></label>
        <button class="destroy">x</button>
    </li>
`;
// document.addEventListener('dragstart', function(event) {
  // const todos = <any> event.target;
  // for(const node of todos.$listContainer.childNodes){
  //   console.log(node.$text)
  // }
//   console.log('todo item', event.target);
// });
export default class TodoItem extends HTMLElement {
  _root: any;
  _index: number;
  _todo_item: IToDoItem;
  dragItem: any[] = [];
  dropItem: any[] = [];
  $item: Element;
  $removeButton: HTMLButtonElement;
  $text: HTMLLabelElement;
  $checkbox: HTMLInputElement;
  constructor() {
    super();
    this._root = this.attachShadow({ 'mode': 'open' });

    this._todo_item = { text: '', checked: false };
    // console.log("TodoItem constructor", this);
  }
  connectedCallback() {
    this._root.appendChild(templateTodoItem.content.cloneNode(true));
    this.$item = this._root.querySelector('.item');
    this.$removeButton = this._root.querySelector('.destroy');
    this.$text = this._root.querySelector('label');
    this.$checkbox = this._root.querySelector('input');

    this.$removeButton.addEventListener('click', (e) => {
      e.preventDefault();
      this.dispatchEvent(new CustomEvent('onRemove', { detail: this._todo_item }));
    });
    this.$checkbox.addEventListener('click', (e) => {
      e.preventDefault();
      if (this._todo_item.checked) this._todo_item.checked = false;
      else this._todo_item.checked = true;
      this.dispatchEvent(new CustomEvent('onToggle', { detail: this._todo_item }));
    });

    this.$text.addEventListener('blur', (event) => {
      event.preventDefault();
      if (this._todo_item.text !== this.$text.innerText.trim()) {
        console.log(this._todo_item.text + ' changed to ' + this.$text.innerText.trim());
        this._todo_item.text = this.$text.innerText.trim();
        this.dispatchEvent(new CustomEvent('onUpdate', { detail: this._todo_item }));
      }
    });
    this.$text.addEventListener('dragstart', (event) => {
      this._todo_item.text = (<HTMLElement>event.target).innerHTML;
      var dragdata = this._todo_item;
      (<any>this.parentNode).dragItem = dragdata;
    });
    this.$text.addEventListener('dragover', function(event) {
      event.preventDefault();
    });
    this.$text.addEventListener('drop', (event) => {
      event.preventDefault();
      if ( (<HTMLElement>event.target).className === 'droptarget') {
        var dragdata = (<any>this.parentNode).dragItem;
        console.log('dragdata', dragdata);
        var dropdata = this._todo_item;
        const dropbeforedata = [dropdata.text, dropdata.checked, dragdata.key];
        dropdata.text = dragdata.text;
        console.log('dropdata', dropdata);

        var array = [{'text': dropbeforedata[0]}, {'checked': dropbeforedata[1]}, {'key':dropbeforedata[2]}],
        dropobject = Object.assign({}, ...array);
        // console.log(dropobject);
        
        this.dispatchEvent(new CustomEvent('onUpdate', { detail: dropdata }));
        dragdata = dropobject;
        // console.log('dragdata', dragdata);
        this.dispatchEvent(new CustomEvent('onUpdate', { detail: dragdata }));
      }
    });
    this._render();
  }

  disconnectedCallback() { }
  static get observedAttributes() {
    return ['text'];
  }
  attributeChangedCallback(name: string, oldValue: string, newValue: string) {
    this._todo_item.text = newValue;
  }
  set index(value) {
    this._index = value;
  }
  get index() {
    return this._index;
  }
  set checked(value) {
    this._todo_item.checked = Boolean(value);
  }
  get checked() {
    return this.hasAttribute('checked');
  }

  get key() {
    return this._todo_item.key;
  }

  set key(value) {
    this._todo_item.key = value;
  }

  _render() {
    if (!this.$item) return;
    this.$text.textContent = this._todo_item.text;
    if (this._todo_item.checked) {
      this.$item.classList.add('completed');
      this.$checkbox.setAttribute('checked', '');
    } else {
      this.$item.classList.remove('completed');
      this.$checkbox.removeAttribute('checked');
    }
  }
}
